#include "rtt_ros_kdl_tools/chain_utils.hpp"
#include <exception>
#include <ros/param.h>

namespace rtt_ros_kdl_tools {

ChainUtilsBase::ChainUtilsBase() {}
bool ChainUtilsBase::init(const std::string &robot_description_name,
                          const std::string &root_link_name,
                          const std::string &tip_link_name,
                          const KDL::Vector gravity_vector) {
  this->robot_description_ros_name = robot_description_name;
  this->root_link_ros_name = root_link_name;
  this->tip_link_ros_name = tip_link_name;
  this->gravity_vector = gravity_vector;
  this->ft_sensor_measure_link = tip_link_name;

  ros::param::get(root_link_name, root_link_name_);

  if (!rtt_ros_kdl_tools::initChainFromROSParamURDF(
          kdl_tree, kdl_chain, robot_description_name, root_link_name,
          tip_link_name)) {
    std::cerr << "Error at rtt_ros_kdl_tools::initChainFromROSParamURDF"
              << std::endl;
    return false;
  }
  if (!rtt_ros_kdl_tools::readJntLimitsFromROSParamURDF(
          kdl_tree, dof, joints_lower_limit_, joints_upper_limit_,
          joints_vel_limit_, joints_effort_limit_, robot_description_name,
          root_link_name)) {
    std::cerr << "Error at rtt_ros_kdl_tools::readJntLimitsFromROSParamURDF"
              << std::endl;
    return false;
  }

  for (int i = 0; i < kdl_chain.getNrOfSegments(); i++)
    seg_names_idx.add(kdl_chain.getSegment(i).getName(), i);

  q.resize(kdl_chain.getNrOfJoints());
  qd.resize(kdl_chain.getNrOfJoints());
  qqd.resize(kdl_chain.getNrOfJoints());

  massMatrix.resize(kdl_chain.getNrOfJoints());
  massMatrixInv.resize(kdl_chain.getNrOfJoints());
  gravityTorque.resize(kdl_chain.getNrOfJoints());
  corioCentriTorque.resize(kdl_chain.getNrOfJoints());
  jacobian.resize(kdl_chain.getNrOfJoints());
  seg_jacobian.resize(kdl_chain.getNrOfJoints());
  seg_jacobian_dot.resize(kdl_chain.getNrOfJoints());
  zero_kdl.resize(kdl_chain.getNrOfJoints());
  zero_kdl.data.setZero();

  tmp_array_pos.resize(kdl_chain.getNrOfJoints());

  ext_wrench_torque.data.setZero(kdl_chain.getNrOfJoints());
  ext_add_torque.data.setZero(kdl_chain.getNrOfJoints());
  ext_torque_all.data.setZero(kdl_chain.getNrOfJoints());

  f_ext.resize(kdl_chain.getNrOfSegments());
  std::fill(f_ext.begin(), f_ext.end(), KDL::Wrench());
  chainjacsolver.reset(new KDL::ChainJntToJacSolver(kdl_chain));
  fksolver.reset(new KDL::ChainFkSolverPos_recursive(kdl_chain));
  fksolvervel.reset(new KDL::ChainFkSolverVel_recursive(kdl_chain));
  dynModelSolver.reset(new KDL::ChainDynParam(kdl_chain, gravity_vector));
  jntToJacDotSolver.reset(new KDL::ChainJntToJacDotSolver(kdl_chain));
  inverseDynamicsSolver.reset(
      new KDL::ChainIdSolver_RNE(kdl_chain, gravity_vector));

  return true;
}

Eigen::VectorXd &ChainUtilsBase::getJointLowerLimit() {
  return this->joints_lower_limit_.data;
}

Eigen::VectorXd &ChainUtilsBase::getJointUpperLimit() {
  return this->joints_upper_limit_.data;
}

Eigen::VectorXd &ChainUtilsBase::getJointVelocityLimits() {
  return this->joints_vel_limit_.data;
}

Eigen::VectorXd &ChainUtilsBase::getJointEffortLimits() {
  return this->joints_effort_limit_.data;
}
// Eigen::VectorXd& ChainUtilsBase::getJointDamping()
// {
//     return this->joint_damping_;
// }

// Eigen::VectorXd& ChainUtilsBase::getJointFriction()
// {
//     return this->joint_friction_;
// }

void ChainUtilsBase::printChain() {
  rtt_ros_kdl_tools::printChain(kdl_chain);

  ROS_INFO_STREAM(joints_name_.size() << " joints limited :");
  for (int i = 0; i < joints_name_.size(); i++)
    ROS_INFO_STREAM("  Joint " << joints_name_[i] << " has limits "
                               << joints_lower_limit_.data[i] << " and "
                               << joints_upper_limit_.data[i]);
}

int ChainUtilsBase::getNrOfSegments() { return kdl_chain.getNrOfSegments(); }
int ChainUtilsBase::getNrOfJoints() { return kdl_chain.getNrOfJoints(); }

const KDL::Segment &ChainUtilsBase::getSegment(unsigned int segment) {
  return kdl_chain.getSegment(segment);
}

Eigen::Affine3d &ChainUtilsBase::getSegmentPosition(unsigned int segment) {
  fksolver->JntToCart(q, seg_pos, segment + 1);
  tf::transformKDLToEigen(seg_pos,seg_pose_eigen);
  return seg_pose_eigen;
}

Eigen::Affine3d &ChainUtilsBase::getSegmentPosition(const std::string &segment_name) {
  return getSegmentPosition(getSegmentIndex(segment_name));
}

Eigen::Matrix<double,6,1> &ChainUtilsBase::getSegmentVelocity(unsigned int segment) {
  fksolvervel->JntToCart(qqd, frame_vel, segment + 1);
  seg_vel = frame_vel.GetTwist();
  tf::twistKDLToEigen(seg_vel,seg_vel_eigen);
  return seg_vel_eigen;
}

Eigen::Matrix<double,6,1> &ChainUtilsBase::getSegmentVelocity(const std::string &segment_name) {
  return getSegmentVelocity(getSegmentIndex(segment_name));
}

void ChainUtilsBase::computeJacobian() {
  chainjacsolver->JntToJac(q, jacobian);
}

Eigen::MatrixXd &ChainUtilsBase::getSegmentJacobian(unsigned int segment) {
  chainjacsolver->JntToJac(q, seg_jacobian, segment + 1);
  jacobian_eigen = seg_jacobian.data;
  return jacobian_eigen;
}

Eigen::MatrixXd &ChainUtilsBase::getSegmentJacobian(const std::string &segment_name) {
  return getSegmentJacobian(getSegmentIndex(segment_name));
}

const std::string &ChainUtilsBase::getSegmentName(unsigned int index) {
  return kdl_chain.getSegment(index).getName();
}

unsigned int ChainUtilsBase::getSegmentIndex(const std::string &name) {
  return seg_names_idx[name];
}

const std::string &ChainUtilsBase::getRootSegmentName() {
  return this->root_link_name_;
}

const std::string &ChainUtilsBase::getTipSegmentName() {
  return this->tip_link_name_;
}

void ChainUtilsBase::getJointLimits(std::vector<std::string> &limited_joints,
                                    Eigen::VectorXd &lower_limits,
                                    Eigen::VectorXd &upper_limits) {
  limited_joints = joints_name_;
  lower_limits = joints_lower_limit_.data;
  upper_limits = joints_upper_limit_.data;
}

Eigen::VectorXd &ChainUtilsBase::getJointPositions() { return q.data; }

Eigen::VectorXd &ChainUtilsBase::getJointVelocities() { return qd.data; }

Eigen::Matrix<double,6,1> &
ChainUtilsBase::getSegmentJdotQdot(const std::string &segment_name) {
  return getSegmentJdotQdot(getSegmentIndex(segment_name));
}

Eigen::Matrix<double,6,1> &ChainUtilsBase::getSegmentJdotQdot(unsigned int segment) {
  jntToJacDotSolver->JntToJacDot(qqd, seg_jdot_qdot, segment + 1);
  tf::twistKDLToEigen(seg_jdot_qdot,seg_jdot_qdot_eigen);
  return seg_jdot_qdot_eigen;
}

Eigen::MatrixXd &ChainUtilsBase::getSegmentJdot(const std::string &segment_name) {
  return getSegmentJdot(getSegmentIndex(segment_name));
}

Eigen::MatrixXd &ChainUtilsBase::getSegmentJdot(unsigned int segment) {
  jntToJacDotSolver->JntToJacDot(qqd, seg_jacobian_dot, segment + 1);
  seg_jdot_eigen = seg_jacobian_dot.data;
  return seg_jdot_eigen;
}

Eigen::MatrixXd &ChainUtilsBase::getInertiaMatrix() {
  return massMatrix.data;
}

Eigen::MatrixXd &ChainUtilsBase::getInertiaInverseMatrix() {
  return massMatrixInv.data;
}

Eigen::Matrix<double, 6, 6> &
ChainUtilsBase::getOperationalSpaceInertiaMatrix() {

  return getOperationalSpaceInertiaMatrix(getRootSegmentName());
}

Eigen::Matrix<double, 6, 6> &
ChainUtilsBase::getOperationalSpaceInertiaMatrix(unsigned int segment) {
  auto jac = getSegmentJacobian(segment);
  auto m_inv_ = getInertiaInverseMatrix();
  Lambda = (jac * m_inv_ * jac.transpose()).inverse();

  return Lambda;
}

Eigen::Matrix<double, 6, 6> &ChainUtilsBase::getOperationalSpaceInertiaMatrix(
    const std::string &segment_name) {

  return getOperationalSpaceInertiaMatrix(getSegmentIndex(segment_name));
}

Eigen::VectorXd &ChainUtilsBase::getCoriolisTorque() {
  return corioCentriTorque.data;
}

Eigen::VectorXd &ChainUtilsBase::getGravityTorque() { return gravityTorque.data; }

KDL::Twist &ChainUtilsBase::getJdotQdot() { return jdot_qdot; }

Eigen::VectorXd &ChainUtilsBase::getJointLowerLimits() {
  return joints_lower_limit_.data;
}
Eigen::VectorXd &ChainUtilsBase::getJointUpperLimits() {
  return joints_upper_limit_.data;
}
std::vector<std::string> &ChainUtilsBase::getLimitedJointNames() {
  return joints_name_;
}

// std::vector< double >& ChainUtilsBase::getJointsFriction()
// {
// 	return this->joints_friction_;
// }

// std::vector< double >& ChainUtilsBase::getJointsDamping()
// {
// 	return this->joints_damping_;
// }

void ChainUtilsBase::updateModel() {
  computeJdotQdot();
  computeGravityTorque();
  computeInertiaMatrix();
  inverseInertiaMatrix();
  computeCorioCentriTorque();
  computeJacobian();
}

RobotDynamicModel ChainUtilsBase::updateRobotModel(const std::string &seg_name ){
  updateModel();
  RobotDynamicModel model;
  model.pose = getSegmentPosition(seg_name);
  model.velocity = getSegmentVelocity(seg_name);
  model.jacobian = getSegmentJacobian(seg_name);
  model.mass = getInertiaMatrix();
  model.gravity = getGravityTorque();
  model.coriolis = getCoriolisTorque();
  model.jdot_qdot = getSegmentJdotQdot(seg_name);
  
  return model;
}

RobotLimits ChainUtilsBase::getRobotJointLimits(){
  RobotLimits limits;

  limits.q_min = getJointLowerLimits();
  limits.q_max = getJointUpperLimits();
  limits.qd_max = getJointVelocityLimits();
  limits.torque_max = getJointEffortLimits();
  
  return limits;
}

Eigen::Matrix<double,6,1> ChainUtilsBase::diff(Eigen::Affine3d x_curr,Eigen::Affine3d x_des)
{
  KDL::Frame x_des_kdl,x_curr_kdl;
  tf::transformEigenToKDL(x_des,x_des_kdl);
  tf::transformEigenToKDL(x_curr,x_curr_kdl);

  KDL::Twist error_kdl = KDL::diff(x_curr_kdl,x_des_kdl);
  Eigen::Matrix<double,6,1> error;
  tf::twistKDLToEigen(error_kdl,error);
  return error;  
}

void ChainUtilsBase::setInertiaMatrixAsDiagonal() {
  massMatrix.data = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>(
      massMatrix.data.diagonal().asDiagonal());
  this->inverseInertiaMatrix();
}

void ChainUtilsBase::computeJdotQdot() {
  jntToJacDotSolver->JntToJacDot(qqd, jdot_qdot);
}
void ChainUtilsBase::computeInertiaMatrix() {
  dynModelSolver->JntToMass(q, massMatrix);
}
void ChainUtilsBase::inverseInertiaMatrix() {
  massMatrixInv.data = massMatrix.data.inverse();
}

KDL::RotationalInertia &
ChainUtilsBase::getSegmentInertiaMatrix(unsigned int seg_idx) {
  rot_intertia =
      this->Chain().getSegment(seg_idx).getInertia().getRotationalInertia();
  return rot_intertia;
}
KDL::RotationalInertia &
ChainUtilsBase::getSegmentInertiaMatrix(const std::string &seg_name) {
  return getSegmentInertiaMatrix(getSegmentIndex(seg_name));
}

void ChainUtilsBase::computeCorioCentriTorque() {
  dynModelSolver->JntToCoriolis(q, qd, corioCentriTorque);
}

void ChainUtilsBase::computeGravityTorque() {
  dynModelSolver->JntToGravity(q, gravityTorque);
}

void ChainUtilsBase::setExternalMeasuredWrench(
    const KDL::Wrench &external_wrench, int segment_number) {
  if (0 <= segment_number && segment_number < f_ext.size()) {
    f_ext[segment_number] = external_wrench;
  } else {
    std::cerr << "Wrong segment number provided " << segment_number
              << std::endl;
  }
}

void ChainUtilsBase::computeExternalWrenchTorque(
    bool compute_gravity /* = true */) {
  return computeExternalWrenchTorque(q.data, compute_gravity);
}

void ChainUtilsBase::computeExternalWrenchTorque(
    const Eigen::VectorXd &jnt_pos, bool compute_gravity /*= true*/) {
  ext_wrench_torque.data.setZero();
  zero_kdl.data.setZero();

  tmp_array_pos.data = jnt_pos;

  inverseDynamicsSolver->CartToJnt(tmp_array_pos, zero_kdl, zero_kdl, f_ext,
                                    ext_wrench_torque);
  // Remove gravity
  if (compute_gravity)
    dynModelSolver->JntToGravity(tmp_array_pos, gravityTorque);

  ext_wrench_torque.data -= getGravityTorque();
}

Eigen::VectorXd &ChainUtilsBase::getExternalWrenchTorque() {
  return ext_wrench_torque.data;
}

void ChainUtilsBase::setExternalAdditionalTorque(
    const Eigen::VectorXd &external_add_torque) {
  if (external_add_torque.size() != ext_add_torque.data.size())
    throw std::runtime_error("Size mismatch");
  this->ext_add_torque.data = external_add_torque;
}

Eigen::VectorXd &ChainUtilsBase::getExternalAdditionalTorque() {
  return this->ext_add_torque.data;
}

Eigen::VectorXd &ChainUtilsBase::getTotalExternalTorque() {
  this->ext_torque_all.data =
      this->ext_add_torque.data + this->ext_wrench_torque.data;
  return ext_torque_all.data;
}

KDL::Frame &ChainUtilsBase::getCartesianPosition(KDL::JntArray q,
                                                 unsigned int segment) {
  fksolver->JntToCart(q, seg_pos, segment + 1);
  return seg_pos;
}

KDL::Frame &ChainUtilsBase::getCartesianPosition(KDL::JntArray q,
                                                 std::string &segment_name) {
  return getCartesianPosition(q, getSegmentIndex(segment_name));
}

Eigen::Matrix<double,6,1> &ChainUtilsBase::getCartesianVelocity(KDL::JntArrayVel qqd,
                                                 unsigned int segment) {
  fksolvervel->JntToCart(qqd, frame_vel, segment + 1);
  seg_vel = frame_vel.GetTwist();
  tf::twistKDLToEigen(seg_vel,seg_vel_eigen);
  return seg_vel_eigen;
}

Eigen::Matrix<double,6,1> &ChainUtilsBase::getCartesianVelocity(KDL::JntArrayVel qqd,
                                                 std::string &segment_name) {
  return getCartesianVelocity(qqd, getSegmentIndex(segment_name));
}

} // namespace rtt_ros_kdl_tools
