[![pipeline status](https://gitlab.inria.fr/auctus/panda/rtt_panda/rtt_ros_kdl_tools/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus/panda/rtt_ros_kdl_tools/commits/master)
[![Quality Gate](https://sonarqube.inria.fr/sonarqube/api/badges/gate?key=auctus:panda:rtt_ros_kdl_tools)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:rtt_ros_kdl_tools)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:rtt_ros_kdl_tools&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:rtt_ros_kdl_tools)

[![Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:rtt_ros_kdl_tools&metric=bugs)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:rtt_ros_kdl_tools)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:rtt_ros_kdl_tools&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:rtt_ros_kdl_tools)
[![Code smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:rtt_ros_kdl_tools&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:rtt_ros_kdl_tools)

[![Line of code](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:rtt_ros_kdl_tools&metric=ncloc)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:rtt_ros_kdl_tools)
[![Comment ratio](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:rtt_ros_kdl_tools&metric=comment_lines_density)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:rtt_ros_kdl_tools)

# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Artt_ros_kdl_tools
- Documentation : https://auctus.gitlabpages.inria.fr/panda/rtt_panda/rtt_ros_kdl_tools/index.html


# rtt_ros_kdl_tools

This code is a fork from https://github.com/kuka-isir/rtt_ros_kdl_tools/

Various RTT/ROS tools for KDL/URDF
