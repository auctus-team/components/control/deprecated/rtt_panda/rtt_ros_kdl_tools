#ifndef __CHAIN_UTILS_BASE__
#define __CHAIN_UTILS_BASE__

#include <boost/scoped_ptr.hpp>
#include <kdl/chain.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainfksolvervel_recursive.hpp>
#include <kdl/chainidsolver_recursive_newton_euler.hpp>
#include <kdl/jntspaceinertiamatrix.hpp>
#include <kdl/tree.hpp>

#include <Eigen/Dense>
#include <eigen_conversions/eigen_kdl.h>
#include <ros/ros.h>
#include <rtt_ros_kdl_tools/chainjnttojacdotsolver.hpp>
#include <rtt_ros_kdl_tools/tools.hpp>
#include <sstream>

namespace rtt_ros_kdl_tools {

class SegmentIndice {
  /* This class is used to bind segment names to the index in the chain */
public:
  const int operator()(const std::string &segment_name) {
    return this->operator[](segment_name);
  }

public:
  const int operator[](const std::string &segment_name) {
    if (seg_idx_names.empty()) {
      std::cerr << "Segment idx is empty ! " << std::endl;
      return -1;
    }
    if (seg_idx_names.count(segment_name) > 0) {
      return seg_idx_names[segment_name];
    } else {
      std::ostringstream ss;
      ss << "Segment name [" << segment_name << "] does not exists";
      throw std::runtime_error(ss.str());
    }
  }

public:
  void add(const std::string &seg_name, int i) { seg_idx_names[seg_name] = i; }

protected:
  std::map<std::string, int> seg_idx_names;
};

struct RobotDynamicModel{
  Eigen::Affine3d pose;
  Eigen::Matrix<double,6,1> velocity;
  Eigen::MatrixXd jacobian;
  Eigen::MatrixXd mass;
  Eigen::VectorXd gravity;
  Eigen::VectorXd coriolis;
  Eigen::VectorXd jdot_qdot;
};

struct RobotLimits{
  Eigen::VectorXd q_min;
  Eigen::VectorXd q_max;
  Eigen::VectorXd qd_max;
  Eigen::VectorXd torque_max;
  Eigen::VectorXd torque_rate_max;
};

class ChainUtilsBase {
public:
  ChainUtilsBase();
  bool init(const std::string &robot_description_name = "robot_description",
            const std::string &root_link = "root_link_",
            const std::string &tip_link = "tip_link_",
            const KDL::Vector gravity_vector = KDL::Vector(0., 0., -9.81));
  KDL::Chain &Chain() { return kdl_chain; }
  KDL::Tree &Tree() { return kdl_tree; }
  /**
   * @brief Prints information about the kdl chain
   */
  void printChain();

  /**
   * @brief Returns the number of segments
   * @return The number of segments
   */
  int getNrOfSegments();
  /**
   * @brief Returns the number of joints
   * @return The number of joints
   */
  int getNrOfJoints();
  /**
   * @brief Gives the kdl segment corresponding to the index
   * @param[in] segment Index number of the segment
   * @param[out] kdl_segment The corresponding kdl segment
   */
  const KDL::Segment &getSegment(unsigned int segment);

  /**
   * @brief Gives the kdl frame position corresponding to the index
   * @param[in] segment Index number of the segment
   * @param[out] kdl_frame The corresponding positon of the segment
   */
  Eigen::Affine3d &getSegmentPosition(unsigned int segment);

  /**
   * @brief Gives the kdl frame position corresponding to its name
   * @param[in] segment_name The segment's name
   * @param[out] kdl_frame The corresponding positon of the segment
   */
  Eigen::Affine3d &getSegmentPosition(const std::string &segment_name);

  /**
   * @brief Gives the kdl twist corresponding to the index
   * @param[in] segment Index number of the segment
   * @param[out] kdl_twist The corresponding twist of the segment
   */
  Eigen::Matrix<double,6,1> &getSegmentVelocity(unsigned int segment);

  /**
   * @brief Gives the kdl twist corresponding to its name
   * @param[in] segment_name The segment's name
   * @param[out] kdl_twist The corresponding twist of the segment
   */
  Eigen::Matrix<double,6,1> &getSegmentVelocity(const std::string &segment_name);

  /**
   * @brief Gives the jacobian expressed in the base frame
   *        with the reference point at the end of the segment
   * @param[in] segment Index number of the segment
   * @param[out] segment The corresponding jacobian of the segment
   */
  Eigen::MatrixXd &getSegmentJacobian(unsigned int segment);

  /**
   * @brief Gives the jacobian expressed in the base frame
   *        with the reference point at the end of the segment
   * @param[in] segment_name The segment's name
   * @param[out] segment The corresponding jacobian of the segment
   */
  Eigen::MatrixXd &getSegmentJacobian(const std::string &segment_name);

  /**
   * @brief Returns the segment name corresponding to the index
   * @param[in] index Index number of the segment
   * @return The segment's name
   */
  Eigen::Matrix<double,6,1> &getSegmentJdotQdot(unsigned int segment);
  Eigen::Matrix<double,6,1> &getSegmentJdotQdot(const std::string &segment_name);
  Eigen::MatrixXd &getSegmentJdot(const std::string &segment_name);
  Eigen::MatrixXd &getSegmentJdot(unsigned int index);
  const std::string &getSegmentName(unsigned int index);
  
  Eigen::Matrix<double,6,1> diff(Eigen::Affine3d x_curr ,Eigen::Affine3d x_des);

  /**
   * @brief Returns the segment index corresponding to its name
   * @param[in] name The segment's name
   * @return The segment's index
   */
  unsigned int getSegmentIndex(const std::string &name);

  /**
   * @brief Returns the name of the root segment
   */
  const std::string &getRootSegmentName();
  /**
   * @brief Returns the name of the tip segment
   */
  const std::string &getTipSegmentName();
  /**
   * @brief Gets the joints limits from the URDF
   * @param[out] limited_joints The names of the joints that have joint limits
   * @param[out] lower_limits The lower limits
   * @param[out] upper_limits The upper limits
   */
  void getJointLimits(std::vector<std::string> &limited_joints,
                      Eigen::VectorXd &lower_limits,
                      Eigen::VectorXd &upper_limits);

  Eigen::VectorXd &getJointLowerLimits();
  Eigen::VectorXd &getJointUpperLimits();
  Eigen::VectorXd &getJointVelocityLimits();
  Eigen::VectorXd &getJointEffortLimits();

  Eigen::VectorXd &getJointLowerLimit();
  Eigen::VectorXd &getJointUpperLimit();

  std::vector<std::string> &getLimitedJointNames();

  /**
   * @brief Gives a kdl JntArray containing the joints position
   * @param[out] q The joints position
   */
  Eigen::VectorXd &getJointPositions();

  /**
   * @brief Gives a kdl JntArray containing the joints velocity
   * @param[out] qd The joints velocity
   */
  Eigen::VectorXd &getJointVelocities();

  /**
   * @brief Gives a kdl JntSpaceInertiaMatrix containing the mass matrix
   * @param[out] massMatrix The mass matrix
   */
  Eigen::MatrixXd &getInertiaMatrix();

  /**
   * @brief Gives a kdl JntSpaceInertiaMatrix containing the inverse of the mass
   * matrix
   * @return massInverseMatrix The inverse mass matrix
   */
  Eigen::MatrixXd &getInertiaInverseMatrix();

  Eigen::Matrix<double, 6, 6> &getOperationalSpaceInertiaMatrix();

  Eigen::Matrix<double, 6, 6> &
  getOperationalSpaceInertiaMatrix(unsigned int segment);

  Eigen::Matrix<double, 6, 6> &
  getOperationalSpaceInertiaMatrix(const std::string &segment_name);

  /**
   * @brief Gives a vectorXd containing the Coriolis torque
   * @param[out] corioCentriTorque The coriolis torque
   */
  Eigen::VectorXd &getCoriolisTorque();

  /**
   * @brief Gives a vectorXd containing the gravitational torque
   * @param[out] gravityTorque The gravitational torque
   */
  Eigen::VectorXd &getGravityTorque();

  /**
   * @brief Gives the JdotQdot of segment in a kdl twist
   * @param[in] segment_name The segment's name
   * @param[out] kdl_twist JdotQdot
   */
  KDL::Twist &getJdotQdot();

  template <class T> void setState(const T &q, const T &qd) {
    setJointPositions(q);
    setJointVelocities(qd);
  }

  template <class T> void setJointPositions(const T &q_) {
    for (unsigned int i = 0; i < kdl_chain.getNrOfJoints() && i < q_.size();
         i++) {
      q(i) = qqd.q(i) = q_[i];
    }
  }
  /**
   * @brief Sets the joint velocity of the model.
   * @param[in] qd_des the joint velocity in rad.
   */
  template <class T> void setJointVelocities(const T &qd_) {
    for (unsigned int i = 0; i < kdl_chain.getNrOfJoints() && i < qd_.size();
         i++) {
      qd(i) = qqd.qdot(i) = qd_[i];
    }
  }
  void updateModel();
  RobotDynamicModel updateRobotModel(const std::string &seg_name );

  RobotLimits getRobotJointLimits();

  /**
   * @brief Modifies the inertia matrix to keep the diagonal elements solely,
   * and recompute the inverse accordingly
   */
  void setInertiaMatrixAsDiagonal();

  /**
   * @brief Add external forces from a force/torque sensor
   * @param[in] external_wrench The external force associate with a link,
   * expressed in the link frame, at the link origin.
   * @param[in] segment_number The link/segment number associated with the ft
   * sensor
   */
  void setExternalMeasuredWrench(const KDL::Wrench &external_wrench,
                                 int segment_number);

  void computeExternalWrenchTorque(bool compute_gravity = true);
  void computeExternalWrenchTorque(const Eigen::VectorXd &jnt_pos,
                                   bool compute_gravity = true);
  Eigen::VectorXd &getExternalWrenchTorque();
  /**
   * @brief Add external torque applied to the robot
   * @param[in] external_wrench The external force associate with a link.
   * @param[in] segment_number The link/segment number associated with the ft
   * sensor
   */
  void setExternalAdditionalTorque(const Eigen::VectorXd &external_add_torque);
  Eigen::VectorXd &getExternalAdditionalTorque();
  Eigen::VectorXd &getTotalExternalTorque();

  /**
   * @brief Inverses the previously computed mass matrix of the model.
   */
  void inverseInertiaMatrix();
  /**
   * @brief Computes the mass matrix of the model.
   */
  KDL::RotationalInertia &getSegmentInertiaMatrix(const std::string &seg_name);
  KDL::RotationalInertia &getSegmentInertiaMatrix(unsigned int seg_idx);

  KDL::Frame &getCartesianPosition(KDL::JntArray q, unsigned int segment);
  KDL::Frame &getCartesianPosition(KDL::JntArray q, std::string &segment_name);
  Eigen::Matrix<double,6,1> &getCartesianVelocity(KDL::JntArrayVel qqd, unsigned int segment);
  Eigen::Matrix<double,6,1> &getCartesianVelocity(KDL::JntArrayVel qqd,
                                   std::string &segment_name);

protected:
  /**
   * @brief Computes the Coriolis, centrifugal and gravity induced joint torque
   * of the model.
   */
  void computeCorioCentriTorque();
  /**
   * @brief Computes the mass matrix of the model.
   */
  void computeInertiaMatrix();
  /**
   * @brief Computes the gravity induced joint torque of the model.
   */
  void computeGravityTorque();
  void computeJacobian();
  void computeJdotQdot();
  /**
   * @brief The root link of the kdl chain
   */
  std::string root_link_name_;

  /**
   * @brief The tip link of the kdl chain
   */
  std::string tip_link_name_;

  /**
   * @brief The joints name
   */
  std::vector<std::string> joints_name_;

  /**
   * @brief The joints lower limits
   */
  KDL::JntArray joints_lower_limit_;
  Eigen::VectorXd joint_lower_limit_;

  /**
   * @brief The joints upper limits
   */
  KDL::JntArray joints_upper_limit_;
  Eigen::VectorXd joint_upper_limit_;

  /**
   * @brief The joints vel limits
   */
  KDL::JntArray joints_vel_limit_;
  Eigen::VectorXd joint_vel_limit_;

  /**
   * @brief The joints effort limits
   */
  KDL::JntArray joints_effort_limit_;
  Eigen::VectorXd joint_effort_limit_;

  // /**
  // * @brief The joints friction
  // */
  // std::vector<double> joints_friction_;
  // Eigen::VectorXd joint_friction_;

  // /**
  // * @brief The joints damping
  // */
  // std::vector<double> joints_damping_;
  // Eigen::VectorXd joint_damping_;

  /**
   * @brief The kinematic tree of the robot.
   */
  KDL::Tree kdl_tree;

  /**
   * @brief The kinematic chain of the robot.
   */
  KDL::Chain kdl_chain;

  /**
   * @brief The joint space mass matrix for the current joint position.
   */
  KDL::JntSpaceInertiaMatrix massMatrix, massMatrixInv;

  /**
   * @brief The Coriolis, centrifugal and gravity induced joint torque
   * for the current joint position and velocity.
   */
  KDL::JntArray corioCentriTorque;

  /**
   * @brief The gravity induced joint torque
   * for the current joint position.
   */
  KDL::JntArray gravityTorque;
  KDL::Jacobian jacobian, seg_jacobian, seg_jacobian_dot;
  KDL::JntArray tmp_array_pos;
  KDL::JntArrayVel qqd;
  KDL::JntArray zero_kdl;
  KDL::Twist jdot_qdot, seg_jdot_qdot;
  KDL::Frame seg_pos;
  KDL::Twist seg_vel;
  KDL::FrameVel frame_vel;
  KDL::JntArray ext_wrench_torque, ext_add_torque, ext_torque_all;
  /**
   * @brief The mapping between segment indice and segment name
   */
  SegmentIndice seg_names_idx;

  /**
   * @brief The current joint position.
   */
  KDL::JntArray q;

  /**
   * @brief The current joint velocity.
   */
  KDL::JntArray qd;
  /**
   * @brief The forward kinematic solver for position
   */
  boost::scoped_ptr<KDL::ChainFkSolverPos_recursive> fksolver;

  /**
   * @brief The forward kinematic solver for velocity
   */
  boost::scoped_ptr<KDL::ChainFkSolverVel_recursive> fksolvervel;

  /**
   * @brief The jacobian solver.
   */
  boost::scoped_ptr<KDL::ChainJntToJacSolver> chainjacsolver;

  /**
   * @brief The dynamic solver.
   */
  boost::scoped_ptr<KDL::ChainDynParam> dynModelSolver;

  /**
   * @brief The JdotQdot solver.
   */
  boost::scoped_ptr<KDL::ChainJntToJacDotSolver> jntToJacDotSolver;
  /**
   * @brief The inverse dynamics solver.
   */
  boost::scoped_ptr<KDL::ChainIdSolver_RNE> inverseDynamicsSolver;

  /**
   * @brief the rosparam argument names
   */
  std::string robot_description_ros_name;
  std::string root_link_ros_name;
  std::string tip_link_ros_name;

  KDL::Vector gravity_vector;
  KDL::RotationalInertia rot_intertia;
  std::string ft_sensor_measure_link;
  std::vector<KDL::Wrench> f_ext;
  Eigen::Matrix<double, 6, 6> Lambda;
  Eigen::MatrixXd jacobian_eigen;
  Eigen::MatrixXd seg_jdot_eigen;
  Eigen::Matrix<double,6,1> seg_vel_eigen;
  Eigen::Matrix<double,6,1> seg_jdot_qdot_eigen;
  Eigen::Affine3d seg_pose_eigen;

  unsigned int dof;
};

} // namespace rtt_ros_kdl_tools

#endif
